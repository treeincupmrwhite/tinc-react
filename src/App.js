import React, { Component } from "react";

import { BrowserRouter, Route, Switch } from 'react-router-dom';

import './fonts/icomoon/style.css';
import './fonts/stylesheet.css';

import './scss/style.scss';

import Main from './pages/main/Main.js';
import About from './pages/about/About.js';
import Services from './pages/services/Services.js';
import Projects from './pages/projects/Projects.js';
import Contacts from './pages/contacts/Contacts.js';
import CompanyPage from './pages/companyPage/CompanyPage.js';
import ScrollToTop from './ScrollToTop.js';

class App extends Component {

    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <ScrollToTop>
                        <Route exact path='/' component={Main} />
                        <Route path='/about' component={About} />
                        <Route path='/services' component={Services} />
                        <Route exact path='/projects' component={Projects} />
                        <Route path='/contacts' component={Contacts} />
                        <Route path='/projects/:company' component={CompanyPage} />
                    </ScrollToTop>
                </Switch>
            </BrowserRouter>
        )
    }
}

export default App;