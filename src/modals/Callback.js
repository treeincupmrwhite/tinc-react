import React, { Component } from "react";

class Modal extends Component {
    render() {
        return (
            <div className="blockMod" id={this.props.modalToOpen}>
                <span className="close" onClick={this.props.close}>
                    <span></span>
                    <span></span>
                </span>
                {
                    (this.props.modalToOpen == "callBack")
                        ?
                        <div className="in">
                            <span className="threeCap">Спросите у нас, мы подскажем</span>
                            <span className="afterCap">для получения ответа заполните и отправьте форму</span>
                            <form className="form" id="ask">
                                <input hidden type="text" name="type" value="callback" />
                                <div className="row">
                                    <div className="item col6 col12-xss itemForm">
                                        <label className="imp">Телефон:</label>
                                        <input name="phone" className="phone" type="text" required />
                                    </div>
                                    <div className="item col6 col12-xss itemForm">
                                        <label className="imp">E-mail:</label>
                                        <input name="email" type="email" required />
                                    </div>
                                    <div className="item col12 itemForm">
                                        <label>Ваш вопрос:</label>
                                        <textarea name="question" required></textarea>
                                    </div>
                                    <div className="item col12 itemForm">
                                        <button className="btnBordRound btnOrange">Отправить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        :
                        (this.props.modalToOpen == "makeOrder")
                            ?
                            <div className="in">
                                <span className="threeCap">Заказ услуги
                                        <span dangerouslySetInnerHTML={{ __html: ' "' + this.props.capTxt + '"' }}></span>
                                </span>
                                <span className="afterCap">для дальнейшей коммуникации заполните и отправьте форму</span>
                                <form className="form" id="order">
                                    <input hidden type="text" name="type" value="order" readOnly />
                                    <input hidden type="text" name="typeWork" readOnly />
                                    <div className="row">
                                        <div className="item col6 col12-xss itemForm">
                                            <label className="imp">Телефон:</label>
                                            <input name="phone" className="phone" type="text" required />
                                        </div>
                                        <div className="item col6 col12-xss itemForm">
                                            <label className="imp">E-mail:</label>
                                            <input name="email" type="email" required />
                                        </div>
                                        <div className="item col12 itemForm">
                                            <label>Комментарий к заказу:</label>
                                            <textarea name="comment"></textarea>
                                        </div>
                                        <div className="item col12 itemForm">
                                            <button className="btnBordRound btnOrange">Отправить</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            :
                            null
                }
                <div id="overlay" onClick={this.props.close}></div>
            </div>
        )
    }
}

export default Modal;