import React, { Component } from "react";

class MakeOrder extends Component {
    render() {
        return (
            <ReactModal
                isOpen={this.state.showModal}
                onRequestClose={this.handleCloseModal}
                shouldCloseOnOverlayClick={true}
                className="tCell middle"
                overlayClassName="table modals"
                closeTimeoutMS={1000}
            >
                <div className="blockMod" id="makeOrder">
                    <span className="close">
                        <span></span>
                        <span></span>
                    </span>
                    <div className="in">
                        <span className="threeCap">Заказ услуги
                                        <span></span>
                        </span>
                        <span className="afterCap">для дальнейшей коммуникации заполните и отправьте форму</span>
                        <form className="form" id="order">
                            <input hidden type="text" name="type" value="order" />
                            <input hidden type="text" name="typeWork" />
                            <div className="row">
                                <div className="item col6 col12-xss itemForm">
                                    <label className="imp">Телефон:</label>
                                    <input name="phone" className="phone" type="text" required />
                                </div>
                                <div className="item col6 col12-xss itemForm">
                                    <label className="imp">E-mail:</label>
                                    <input name="email" type="email" required />
                                </div>
                                <div className="item col12 itemForm">
                                    <label>Комментарий к заказу:</label>
                                    <textarea name="comment"></textarea>
                                </div>
                                <div className="item col12 itemForm">
                                    <button className="btnBordRound btnOrange">Отправить</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="overlay"></div>
                </div>
            </ReactModal>
        )
    }
}

export default MakeOrder;