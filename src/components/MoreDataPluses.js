import React, { Component } from "react";

import { Link } from 'react-router-dom';

class MoreDataPluses extends Component {
    render() {
        return (
            <div className="table">
                <div className="tCell middle">
                    <p>{this.props.content.text}</p>
                    <Link to="/projects" className="btnBordRound btnOrange">{this.props.content.linkText}</Link>
                </div>
            </div>

        )
    }
}

export default MoreDataPluses;