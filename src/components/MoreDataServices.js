import React, { Component } from "react";

import ReactModal from 'react-modal';

import Modal from '../modals/Callback.js';

ReactModal.setAppElement('#root');

class MoreDataServices extends Component {
    constructor(props) {
        super(props);
        this.state = { openMod: false, modalOpened: '', openClass: 'modalsScroll' };

        this.handleCloseModal = this.handleCloseModal.bind(this);
    }

    handleOpenModal(modal) {
        this.setState({ showModal: true, modalOpened: modal, openClass: 'modalsScroll open' });
        console.log(this.props)
    }

    handleCloseModal() {
        this.setState({ showModal: false, openClass: 'modalsScroll' });
    }

    render() {
        return (
            <div className="table">
                {
                    this.props.content.type == "parametres"
                        ?
                        <div className="tCell middle">
                            <ul className="devices">
                                {
                                    this.props.content.devices.map((item, index) => {
                                        return (
                                            <li key={index}><span className={item.icon}></span></li>
                                        )
                                    })
                                }
                            </ul>
                            <ul className="params">
                                {
                                    this.props.content.params.map((item, index) => {
                                        return (
                                            <li className={`cfix ${item.costClass ? item.costClass : ""}`} key={index}>
                                                <span className="cap">{item.caption}</span>
                                                <span className={item.costClass ? "cost" : null} dangerouslySetInnerHTML={{ __html: item.numbers }}></span>
                                            </li>
                                        )
                                    })
                                }
                            </ul>
                            <span className="btnBordRound btnOrange" onClick={this.handleOpenModal.bind(this, 'makeOrder')}>Заказать</span>
                        </div>
                        :
                        <div className="tCell middle">
                            <p dangerouslySetInnerHTML={{ __html: this.props.content.text }}></p>
                            <span className="btnBordRound btnOrange" onClick={this.handleOpenModal.bind(this, 'callBack')}>Связаться</span>
                        </div>
                }
                <ReactModal
                    isOpen={this.state.showModal}
                    onRequestClose={this.handleCloseModal}
                    shouldCloseOnOverlayClick={true}
                    className="tCell middle"
                    overlayClassName="table modals"
                    closeTimeoutMS={1000}
                    portalClassName={this.state.openClass}
                >
                    <Modal modalToOpen={this.state.modalOpened} close={this.handleCloseModal} capTxt={this.props.cap} />
                </ReactModal>
            </div>
        )
    }
}

export default MoreDataServices;