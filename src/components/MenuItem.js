import React, { Component } from "react";

import {NavLink} from 'react-router-dom';

class MenuItem extends Component {
    render() {
        const val = this.props.values;
        return (
            <NavLink exact activeClassName="active" to={val.link}>{val.title}</NavLink>
        )
    }
}

export default MenuItem;