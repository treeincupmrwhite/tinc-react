import React, { Component } from "react";

class CapBlock extends Component {
    render() {
        return (
            <div className={`capBlock ${this.props.classToBlock}`}>
                {
                    this.props.beforeCaption
                        ?
                        <span className="beforeCap">{this.props.beforeCaption}</span>
                        :
                        null
                }
                {
                    this.props.beforeIcon
                        ?
                        <span className={`beforeIcon ${this.props.beforeIcon}`}></span>
                        :
                        null
                }
                {
                    this.props.captionType == "secondSpan"
                    ?
                    <span className="secCap">{this.props.captionTitle}</span>
                    :
                    this.props.captionType == "thirdSpan"
                    ?
                    <span className="thirdCap">{this.props.captionTitle}</span>
                    :
                    this.props.captionType == "secondCap"
                    ?
                    <h1 className="secCap">{this.props.captionTitle}</h1>
                    :
                    null
                }
                {
                    this.props.afterCaption
                        ?
                        <p className="afterSecCapText" dangerouslySetInnerHTML={{ __html: this.props.afterCaption }} />
                        :
                        null
                }
            </div>
        )
    }
}

export default CapBlock;