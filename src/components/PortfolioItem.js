import React, { Component } from "react";

import InvisLink from './InvisLink.js';

class PortfolioItem extends Component {
    render() {
        return (
            <InvisLink
                classToBlock="portfItem table"
                linkHref={this.props.content.linkHref}
                linkTitle={this.props.content.linkTitle}
            >
                {
                    this.props.content.anhor
                        ?
                        <span id={this.props.content.anhor} className="linkAnhor"></span>
                        :
                        null
                }
                <div className="image" style={{ backgroundImage: `url(${this.props.content.image})` }}>
                    <img src={this.props.content.image} alt="" />
                </div>
                <div className="cont tCell middle textCenter">
                    <h3 className="threeCap">{this.props.content.caption}</h3>
                    <span className="addresPortfol">{this.props.content.addr}</span>
                </div>
                <span className="icon-arrow-up-right"></span>
            </InvisLink>
        )
    }
}

export default PortfolioItem;