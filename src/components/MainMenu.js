import React, { Component } from "react";

import MenuItem from './MenuItem.js';

const listMenu = [
    {
        title: "Главная",
        link: "/",
    },
    {
        title: "О нас",
        link: "/about",
    },
    {
        title: "Услуги",
        link: "/services",
    },
    {
        title: "Проекты",
        link: "/projects",
    },
    {
        title: "Контакты",
        link: "/contacts",
    },
]

class MainMenu extends Component {
    constructor(props) {
        super(props);

        window.innerWidth < 768
        ?
        this.state = {menuOpen: false}
        :
        this.state = {menuOpen: true}
    }

    handleOpenMenu(){
        this.setState({menuOpen: !this.state.menuOpen})
    }

    render() {
        return (
            <div className="mainMenu right">
                <span className={`menuBtn showXS ${this.state.menuOpen === true ? 'selected' : ''}`} onClick={this.handleOpenMenu.bind(this)}>
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
                <ul className={this.state.menuOpen === true ? 'active' : ''}>
                    {
                        
                        listMenu.map((item, index) => {
                            return (
                                <li className="item" key={index}>
                                    <MenuItem values={item} />
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        )
    }
}

export default MainMenu;