import React, { Component } from "react";

import { Link } from 'react-router-dom';

class InvisLink extends Component {
    render() {
        return (
            <div className={`invisLink ${this.props.classToBlock}`}>
                <Link to={this.props.linkHref}>{this.props.linkTitle}</Link>
                {this.props.children}
            </div>
        )
    }
}

export default InvisLink;