import React, { Component } from "react";

class Caption extends Component {
    render() {
        switch (this.props.captionType) {
            case 'firstCap':
                return <h1 className={`firstCap ${this.props.classToBlock}`}>{this.props.captionTitle}</h1>
            case 'secondCap':
                return <h2 className={`secCap ${this.props.classToBlock}`}>{this.props.captionTitle}</h2>
            default:
                null
        }
    }
}

export default Caption;