import React, { Component } from "react";

import { Link } from 'react-router-dom';

import CapBlock from '../CapBlock.js';
import PortfolioItem from '../PortfolioItem.js';

// const portfolioList = [
//     {
//         classItem: "",
//         linkHref: "/project/atevi",
//         linkTitle: "Сайт atevi.by",
//         image: "/img/atevi/atevi.jpg",
//         caption: "Atevi",
//         addr: "Atevi.by"
//     },
//     {
//         classItem: "",
//         linkHref: "/project/uspeh",
//         linkTitle: "Сайт uspehbrest.by",
//         image: "/img/uspeh/uspeh.jpg",
//         caption: "Успех",
//         addr: "uspehbrest.by"
//     },
//     {
//         classItem: "",
//         linkHref: "/project/teamwork",
//         linkTitle: "Сайт studio.com",
//         image: "/img/tw/tw.jpg",
//         caption: "TeamWork",
//         addr: "teamwork-studio.com"
//     },
//     {
//         classItem: "hideXS",
//         linkHref: "/project/brw",
//         linkTitle: "Сайт brw.by",
//         image: "/img/brw/brw.jpg",
//         caption: "BRW",
//         addr: "brw.by"
//     },
//     {
//         classItem: "hideXS",
//         linkHref: "/project/autobax",
//         linkTitle: "Сайт autobax.by",
//         image: "/img/autob/autob.jpg",
//         caption: "autobax",
//         addr: "autobax.by"
//     },
//     {
//         classItem: "hideXS",
//         linkHref: "/project/mustang",
//         linkTitle: "Сайт mustangtk.com",
//         image: "/img/mustang/mustang.jpg",
//         caption: "Mustang",
//         addr: "mustangtk.com"
//     },
// ]

class Portfolio extends Component {
    render() {
        return (
            <section className={`portfolio ${this.props.classToBlock}`}>
                <div className="container">
                    {
                        this.props.content.capBlock
                            ?
                            <CapBlock
                                captionType="thirdSpan"
                                captionTitle={this.props.content.capBlock.titleCaption}
                            />
                            :
                            null
                    }
                    <div className="grid">
                        {
                            this.props.content.portfolioList.map((item, index) => {
                                return (
                                    <div className={`item ${item.classItem}`} key={index}>
                                        <PortfolioItem content={item} />
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
                {
                    this.props.content.buttonToAll
                        ?
                        <div className="container center">
                            <Link to="/projects" className="btnBorder btnOrange">Все проекты</Link>
                        </div>
                        :
                        null
                }
            </section>
        )
    }
}

export default Portfolio;