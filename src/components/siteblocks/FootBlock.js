import React, { Component } from "react";

import ReactModal from 'react-modal';

import CapBlock from '../CapBlock.js';
import Modal from '../../modals/Callback.js';

ReactModal.setAppElement('#root');

class FootBlock extends Component {
    constructor(props) {
        super(props);
        this.state = {openMod: false, modalOpened: '', openClass: 'modalsScroll'};
  
        this.handleCloseModal = this.handleCloseModal.bind(this);
    }
      
    handleOpenModal(item) {
        this.setState({ showModal: true, modalOpened: item, openClass: 'modalsScroll open' });
    }
      
    handleCloseModal () {
        this.setState({ showModal: false, openClass: 'modalsScroll' });
    }

    render() {
        return (
            <section className="footBlock dark">
                <div className="container center">
                    <div className="smallContainer">
                        <CapBlock 
                            classToBlock="center"
                            beforeIcon="icon-chat" 
                            afterCaption="Заказать вёрстку, получить консультацию по вашему сайту вы можете
                                <br class='hideMD'>по адресу электронной почты
                                <a href='mailto:info@tinc.by'>info@tinc.by</a> или заполните заявку" />
                    </div>
                    <button className="btnBordRound btnOrange" onClick={this.handleOpenModal.bind(this, 'callBack')} datamodal="callBack">Оставить заявку</button>
                </div>
                <ReactModal
                    isOpen={this.state.showModal}
                    onRequestClose={this.handleCloseModal}
                    shouldCloseOnOverlayClick={true}
                    className="tCell middle"
                    overlayClassName="table modals"
                    closeTimeoutMS={1000}
                    portalClassName={this.state.openClass}
                >
                    <Modal modalToOpen={this.state.modalOpened} close={this.handleCloseModal} />
                </ReactModal>
            </section>
        )
    }
}

export default FootBlock;