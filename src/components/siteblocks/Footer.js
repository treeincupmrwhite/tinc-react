import React, { Component } from "react";

import InvisLink from '../InvisLink.js';

class Footer extends Component {
    render() {
        return (
            <footer>
                <div className="lineCont">
                    <div className="container">
                        <div className="table">
                            <div className="tCell middle col4 hideXS">
                                <InvisLink
                                    classToBlock="logo left"
                                    linkHref="/"
                                    linkTitle="Вёрстка сайтов"
                                />
                            </div>
                            <div className="tCell middle col4 center">
                                <a href="tel:+375298952329" className="phone">+375(29)&nbsp;895-23-29</a>
                            </div>
                            <div className="tCell middle col4 right">
                                <ul>
                                    <li className="invisLink">
                                        <span className="icon-skype"></span>
                                        <a href="skype:treeincupmri@gmail.com?chat">Skype</a>
                                    </li>
                                    <li className="invisLink">
                                        <span className="icon-mail"></span>
                                        <a href="mailto:treeincupmri@gmail.com">treeincupmri@gmail.com</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="lineCopy">
                    <div className="container">
                        <p>© 2015-2018 Студия web-вёрстки и поддержке сайтов. УНП 291404593</p>
                    </div>
                </div>
            </footer>
        )
    }
}

export default Footer;