import React, { Component } from "react";

import { Tab, Tabs, TabList, TabPanel } from "react-tabs";

import { Link } from 'react-router-dom';

import Lightbox from 'lightbox-react';
import 'lightbox-react/style.css';

class PortfolioProject extends Component {
    constructor(props) {
        super(props);

        this.state = {
            photoIndex: 0,
            isOpen: false,
        };
    }
    render() {
        const { photoIndex, isOpen } = this.state;
        return (
            <section className="portfolioProject noPaddBott">
                <div className="container">
                    <Tabs className="tabs">
                        <div className="tabHead">
                            <TabList className="table full">
                                <Tab className="tCell middle invisLink color1Hover color1Active color8Back color7Border">
                                    <span className="icon-desktop"></span>
                                </Tab>
                                <Tab className="tCell middle invisLink color1Hover color1Active color8Back color7Border">
                                    <span className="icon-tablet2"></span>
                                </Tab>
                                <Tab className="tCell middle invisLink color1Hover color1Active color8Back color7Border">
                                    <span className="icon-phone2"></span>
                                </Tab>
                            </TabList>
                        </div>
                        <div className="tabCont anim fadeIn">
                            <TabPanel className="item ui-tabs-panel">
                                <div className="portfolioList cfix">
                                    {
                                        this.props.content.desktop.map((item, index) => {
                                            return (
                                                <div className="item" key={index}>
                                                    <div className="invisLink">
                                                        <img src={item.image} />
                                                        <a type="button" onClick={() => this.setState({ photoIndex: index, isOpen: true })}></a>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                    {isOpen && (
                                        <Lightbox
                                            mainSrc={this.props.content.desktop[this.state.photoIndex].link}
                                            nextSrc={this.props.content.desktop[(photoIndex + 1) % this.props.content.desktop.length].link}
                                            prevSrc={this.props.content.desktop[(photoIndex + this.props.content.desktop.length - 1) % this.props.content.desktop.length].link}
                                            onCloseRequest={() => this.setState({ isOpen: false })}
                                            onMovePrevRequest={() =>
                                                this.setState({
                                                  photoIndex: (photoIndex + this.props.content.desktop.length - 1) % this.props.content.desktop.length,
                                                })
                                              }
                                            onMoveNextRequest={() =>
                                                this.setState({
                                                  photoIndex: (photoIndex + 1) % this.props.content.desktop.length,
                                                })
                                              }
                                        />
                                    )}
                                </div>
                            </TabPanel>
                            <TabPanel className="item ui-tabs-panel">
                                <div className="portfolioList cfix">
                                    {
                                        this.props.content.tablet.map((item, index) => {
                                            return (
                                                <div className="item" key={index}>
                                                    <div className="invisLink">
                                                        <img src={item.image} />
                                                        <a type="button" onClick={() => this.setState({ photoIndex: index, isOpen: true })}></a>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                    {isOpen && (
                                        <Lightbox
                                        mainSrc={this.props.content.tablet[this.state.photoIndex].link}
                                        nextSrc={this.props.content.tablet[(photoIndex + 1) % this.props.content.tablet.length].link}
                                        prevSrc={this.props.content.tablet[(photoIndex + this.props.content.tablet.length - 1) % this.props.content.tablet.length].link}
                                        onCloseRequest={() => this.setState({ isOpen: false })}
                                        onMovePrevRequest={() =>
                                            this.setState({
                                              photoIndex: (photoIndex + this.props.content.tablet.length - 1) % this.props.content.tablet.length,
                                            })
                                          }
                                        onMoveNextRequest={() =>
                                            this.setState({
                                              photoIndex: (photoIndex + 1) % this.props.content.tablet.length,
                                            })
                                          }
                                    />
                                    )}
                                </div>
                            </TabPanel>
                            <TabPanel className="item ui-tabs-panel">
                                <div className="portfolioList cfix">
                                    {
                                        this.props.content.mobile.map((item, index) => {
                                            return (
                                                <div className="item" key={index}>
                                                    <div className="invisLink">
                                                        <img src={item.image} />
                                                        <a type="button" onClick={() => this.setState({ photoIndex: index, isOpen: true })}></a>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                    {isOpen && (
                                        <Lightbox
                                        mainSrc={this.props.content.mobile[this.state.photoIndex].link}
                                        nextSrc={this.props.content.mobile[(photoIndex + 1) % this.props.content.mobile.length].link}
                                        prevSrc={this.props.content.mobile[(photoIndex + this.props.content.mobile.length - 1) % this.props.content.mobile.length].link}
                                        onCloseRequest={() => this.setState({ isOpen: false })}
                                        onMovePrevRequest={() =>
                                            this.setState({
                                              photoIndex: (photoIndex + this.props.content.mobile.length - 1) % this.props.content.mobile.length,
                                            })
                                          }
                                        onMoveNextRequest={() =>
                                            this.setState({
                                              photoIndex: (photoIndex + 1) % this.props.content.mobile.length,
                                            })
                                          }
                                    />
                                    )}
                                </div>
                            </TabPanel>
                        </div>
                    </Tabs>
                    <div className="textBlock container">
                        <div className="innerBlock">
                            <div className="smallContainer">
                                <p className="textCenter" dangerouslySetInnerHTML={{ __html: this.props.content.textBlock }}></p>
                                <Link to="/projects" className="btnBorder btnOrange center">Все проекты</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default PortfolioProject;