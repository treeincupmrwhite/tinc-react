import React, { Component } from "react";

import MoreDataServices from '../MoreDataServices.js';
import MoreDataPluses from '../MoreDataPluses.js';

class Pluses extends Component {
    render() {
        return (
            <section className={`pluses ${this.props.classToBlock}`}>
                <div className="container">
                    <div className="row noPadd">
                        {
                            this.props.content.map((item, index) => {
                                return (
                                    <div className="item col4 col6-md col12-sm col6-xs col12-xss" key={index}>
                                        <div className="openDataBlock">
                                            <div className="smallData table">
                                                <div className="tCell middle">
                                                    <span className={item.openDataBlock.icon}></span>
                                                    <span className="beforeCap">{item.openDataBlock.beforeCaption}</span>
                                                    <h2 className="secCap" dangerouslySetInnerHTML={{ __html: item.openDataBlock.caption }}></h2>
                                                </div>
                                            </div>
                                            <div className="moreData">
                                                <div className="inner">
                                                    {
                                                        this.props.classToBlock == "services"
                                                            ?
                                                            <MoreDataServices content={item.moreData} cap={item.openDataBlock.caption} />
                                                            :
                                                            <MoreDataPluses content={item.moreData} cap={item.openDataBlock.caption} />
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
                {
                    this.props.textBlock
                        ?
                        <div className="textBlock container">
                            <div className="innerBlock">
                                <div className="smallContainer">
                                    <p className="textCenter" dangerouslySetInnerHTML={{ __html: this.props.textBlock.text }}></p>
                                </div>
                            </div>
                        </div>
                        :
                        null
                }
            </section>
        )
    }
}

export default Pluses;