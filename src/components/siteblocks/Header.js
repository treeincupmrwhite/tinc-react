import React, { Component } from "react";

import InvisLink from '../InvisLink.js';
import MainMenu from '../MainMenu.js';

class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            isHide: false
        };

        this.handleScroll = this.handleScroll.bind(this)
    }
    handleScroll() {
        const headHeight = document.getElementsByTagName("header")[0].offsetHeight;
        const headLineHeight = document.getElementsByClassName("headLineMenu")[0].offsetHeight;
        let scrolled = window.pageYOffset || document.documentElement.scrollTop;
        scrolled > (headHeight - headLineHeight)
            ?
            this.setState({ isHide: true })
            :
            this.setState({ isHide: false })
    }
    componentDidMount() {
        setTimeout(() => this.setState({ open: true }), 100);
        window.addEventListener('scroll', this.handleScroll);
    }
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    render() {
        const classAdding = this.props.pageIs == undefined
            ? "project"
            : this.props.pageIs == "Главная"
                ? "mainPage"
                : "inner"
        return (
            <header className={`innerBigHead ${classAdding}`}>
                <div className={`headLineMenu ${this.state.isHide ? "active" : ""}`}>
                    <div className="container">
                        <div className="cfix">
                            {
                                (this.props.pageIs == "Главная" || this.props.pageIs == undefined)
                                    ?
                                    <InvisLink
                                        classToBlock="logo left"
                                        linkHref="/"
                                        linkTitle="Главная"
                                    >
                                        <img src="/img/logo_b.svg" alt="Логотип" />
                                        <h1 className="linkInvise">Адаптивная вёрстка сайтов</h1>
                                    </InvisLink>
                                    :
                                    <h1 className="firstCap left">{this.props.pageIs}</h1>
                            }
                            <MainMenu />
                        </div>
                    </div>
                </div>
                <section className={`mainPageFirst table logoBl ${this.state.open ? "active" : ""}`}>
                    <div className="tCell middle">
                        <div className="container">
                            <InvisLink
                                classToBlock="logo"
                                linkHref="/"
                                linkTitle="Главная"
                            >
                                <img src="/img/logo_b.svg" alt="Логотип" />
                                <span>Верстаем для вас</span>
                            </InvisLink>
                        </div>
                    </div>
                </section>
            </header>
        )
    }
}



export default Header;