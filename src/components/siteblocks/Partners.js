import React, { Component } from "react";

import CapBlock from '../CapBlock.js';

const partnersList = [
    {
        classLogo: "dark",
        image: "/img/atevi_logo.png"
    },
    {
        classLogo: "",
        image: "/img/ww_logo.png"
    },
    {
        classLogo: "",
        image: "/img/tw_logo.png"
    },
    {
        classLogo: "dark",
        image: "/img/nw_logo.png"
    },
]

class Partners extends Component {
    render() {
        return (
            <section className="partners">
                <div className="container">
                    <div className="innerBlock">
                        <div className="smallContainer">
                            <CapBlock
                                captionType="secondSpan"
                                captionTitle="С нами уже работают"
                                afterCaption="Не первый год качеству нашей работы доверяют IT компании, для которых мы готовы расти, развиваться и совершенствоваться в области Вёрстки"
                            />
                            <div className="grid">
                                {
                                    partnersList.map((item, index) => {
                                        return (
                                            <div className="item col3 col6-xs" key={index}>
                                                <span className={`compLogo ${item.classLogo}`}>
                                                    <span className="img" style={{ backgroundImage: `url(${item.image})` }}></span>
                                                </span>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Partners;