import React, { Component } from "react";

class ContactsBlock extends Component {
    render() {
        return (
            <section className="contacts grayBack">
                <div className="container">
                    <div className="row">
                        <div className="item right col5 col12-sm">
                            <div className="textBlock">
                                <ul>
                                    <li>
                                        <span className="thirdCap">Телефон:</span>
                                        <a href="tel:+375298952329">+375(29) 895-23-29 МТС</a>
                                    </li>
                                    <li>
                                        <span className="thirdCap">E-mail:</span>
                                        <a href="mailto:info@tinc.by">info@tinc.by</a>
                                    </li>
                                    <li>
                                        <span className="thirdCap">Режим работы и приём звонков:</span>
                                        <p>Понедельник - пятница с 09:00 до 18:00</p>
                                        <p>Суббота с 10:00 до 16:00</p>
                                        <p>Воскресенье - выходной день</p>
                                        <p>Заказ проектов через сайт круглосуточно</p>
                                    </li>
                                    <li>
                                        <span className="thirdCap">Способ оплаты:</span>
                                        <p>по безналичному расчёту</p>
                                    </li>
                                    <li>
                                        <span className="thirdCap">Наш адрес:</span>
                                        <p>ул. Комсомольская, д. 40, оф. 302, 224005, г. Брест</p>
                                    </li>
                                    <li>
                                        <span className="thirdCap">Банковские реквизиты:</span>
                                        <p>УНП 291404593</p>
                                        <p>р/с BY25ALFA30132115470040270000 в ЗАО «Альфа-банк»,
                                        <br class="hideMD" />SWIFT – ALFABY2X, адрес банка ул. Сурганова, д. 43, оф. 47, 220013, г. Минск, УНП 101541947, ОКПО 37526626.</p>
                                    </li>
                                    <li>
                                        <span className="thirdCap">Руководитель
                                            <span>Андрей Андреевич Васильев</span>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="item col7 col12-sm">
                            <iframe className="map" src="https://yandex.by/map-widget/v1/?z=12&ol=biz&oid=8682551880" width="100%" frameborder="0"></iframe>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default ContactsBlock;