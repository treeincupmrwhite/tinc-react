import React, { Component } from "react";

import { Link } from 'react-router-dom';

import CapBlock from '../CapBlock';

class AboutBlock extends Component {
    render() {
        return (
            <section className="aboutBlock grayBack">
                <div className="smallContainer">
                    <CapBlock
                        captionType = "secondSpan"
                        classToBlock="center"
                        beforeCaption={this.props.content.capBlock.beforeCaption}
                        captionTitle={this.props.content.capBlock.titleCaption}
                        beforeIcon={this.props.content.capBlock.beforeIcon}
                        afterCaption={this.props.content.capBlock.afterCaption}
                    />
                    <div className="textBlock center">
                        <p dangerouslySetInnerHTML={{ __html: this.props.content.textBlock.text }} />
                        {
                            this.props.content.textBlock.link
                                ?
                                <Link
                                    to={this.props.content.textBlock.hrefLink}
                                    className="btnBordRound btnOrange"
                                >
                                    {this.props.content.textBlock.link}
                                </Link>
                                :
                                null
                        }
                    </div>
                    {
                        this.props.content.companyLogo
                        ?
                        <span className="compLogo dark">
                            <span className="img" style={{ backgroundImage: `url(${this.props.content.companyLogo.image})` }}></span>
                        </span>
                        :
                        null
                    }
                </div>
            </section>
        )
    }
}

export default AboutBlock;