const servicesContent = [
    {
        openDataBlock: {
            icon: "icon-clipboard",
            beforeCaption: "как положено",
            caption: "мы отвечаем<br class='hideXS'> за свою работу"
        },
        moreData: {
            text: "Любая выполненная работа подтвергается тщательному тестированию, а так же максимально поддержкой мы гарантируем, что ваш сайт будет хорош",
            linkText: "Посмотреть проекты"
        }
    },
    {
        openDataBlock: {
            icon: "icon-stats-bars",
            beforeCaption: "всё на максимум",
            caption: "Качество<br class='hideXS'> для каждого"
        },
        moreData: {
            text: "Качество - это продукт, который вы желаете и заслуживаете, сайт будет хорош на всех устройствах, а приложение стабильное и быстрое",
            linkText: "Проверить"
        }
    },
    {
        openDataBlock: {
            icon: "icon-rocket",
            beforeCaption: "время не стоит",
            caption: "Новые технологии<br class='hideMD'> они уже тут"
        },
        moreData: {
            text: "Именно используя новые и современные технологие, мы можем вам гарантировать, что ваш продукт будет максимально эффективный",
            linkText: "Посмотреть проекты"
        }
    },
]

export default servicesContent