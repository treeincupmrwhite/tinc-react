const portfolioContent = {
    capBlock: {
        titleCaption: "Наши последние проекты"
    },
    buttonToAll: "yes",
    portfolioList: [
        {
            classItem: "",
            linkHref: "/project/atevi",
            linkTitle: "Сайт atevi.by",
            image: "/img/atevi/atevi.jpg",
            caption: "Atevi",
            addr: "Atevi.by"
        },
        {
            classItem: "",
            linkHref: "/project/uspeh",
            linkTitle: "Сайт uspehbrest.by",
            image: "/img/uspeh/uspeh.jpg",
            caption: "Успех",
            addr: "uspehbrest.by"
        },
        {
            classItem: "",
            linkHref: "/project/teamwork",
            linkTitle: "Сайт studio.com",
            image: "/img/tw/tw.jpg",
            caption: "TeamWork",
            addr: "teamwork-studio.com"
        },
        {
            classItem: "hideXS",
            linkHref: "/project/brw",
            linkTitle: "Сайт brw.by",
            image: "/img/brw/brw.jpg",
            caption: "BRW",
            addr: "brw.by"
        },
        {
            classItem: "hideXS",
            linkHref: "/project/autobax",
            linkTitle: "Сайт autobax.by",
            image: "/img/autob/autob.jpg",
            caption: "autobax",
            addr: "autobax.by"
        },
        {
            classItem: "hideXS",
            linkHref: "/project/mustang",
            linkTitle: "Сайт mustangtk.com",
            image: "/img/mustang/mustang.jpg",
            caption: "Mustang",
            addr: "mustangtk.com"
        },
    ]
}

export default portfolioContent;