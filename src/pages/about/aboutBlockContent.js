const aboutBlockContent = {
    capBlock: {
        beforeCaption: "Почему tinc",
        titleCaption: "TreeInCup"
    },
    textBlock: {
        text: "Разбираясь в бизнесе и технологиях, запускаем сайты с чистым кодом. Находимся в Бресте и работаем с клиентами из СНГ и Европы. Пониманием потребности бизнеса и предлагаем услуги по web разработке: верстка web-сайтов, адаптивная верстка, оптимизация web-сайтов, статичная верстка, работа с кодом на CMS. Используем современные технологии: HTML5, CSS 3, JavaScript, jQuery, SASS, SCSS, Bootstrap, Git.",
        link: "Услуги для Вас",
        hrefLink: "/services"
    }
}

export default aboutBlockContent;