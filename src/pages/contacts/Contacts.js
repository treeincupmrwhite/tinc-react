import React, { Component } from 'react';

import Header from '../../components/siteblocks/Header.js';
import Footer from '../../components/siteblocks/Footer.js';
import ContactsBlock from '../../components/siteblocks/ContactsBlock.js';
import FootBlock from '../../components/siteblocks/FootBlock.js';

class Contacts extends Component {
    render() {
        const pageName = "Контакты";
        return (
            <div>
                <Header pageIs={pageName} />
                <ContactsBlock />
                <FootBlock />
                <Footer />
            </div>
        );
    }
}

export default Contacts;