import React, { Component } from 'react';

import Header from '../../components/siteblocks/Header.js';
import Footer from '../../components/siteblocks/Footer.js';
import FootBlock from '../../components/siteblocks/FootBlock.js';
import Pluses from '../../components/siteblocks/Pluses';
import servicesContent from './servicesContent';
import textBlock from './textBlock';

class Services extends Component {
    render() {
        const pageName = "Услуги";
        return ( 
            <div>
                <Header pageIs={pageName} />
                <Pluses classToBlock="services" textBlock={textBlock} content={servicesContent} />
                <FootBlock />
                <Footer />
            </div>
        );
    }
}

export default Services;