const portfolioContent = [
    {
        portfolioList: [
            {
                classItem: "",
                anhor: "atevi",
                linkHref: "/projects/atevi",
                linkTitle: "Сайт atevi.by",
                image: "/img/atevi/atevi.jpg",
                caption: "Atevi",
                addr: "Atevi.by"
            },
            {
                classItem: "",
                anhor: "uspeh",
                linkHref: "/projects/uspeh",
                linkTitle: "Сайт uspehbrest.by",
                image: "/img/uspeh/uspeh.jpg",
                caption: "Успех",
                addr: "uspehbrest.by"
            },
            {
                classItem: "",
                anhor: "imns",
                linkHref: "/projects/hoster",
                linkTitle: "Сайт hoster.by",
                image: "/img/hoster/hoster.jpg",
                caption: "Hoster",
                addr: "hoster.by"
            },
            {
                classItem: "",
                anhor: "tw",
                linkHref: "/projects/teamwork",
                linkTitle: "Сайт teamwork-studio.com",
                image: "/img/tw/crop/tw.jpg",
                caption: "TeamWork",
                addr: "teamwork-studio.com"
            },
            {
                classItem: "",
                anhor: "brw",
                linkHref: "/projects/brw",
                linkTitle: "Сайт brw.by",
                image: "/img/brw/brw.jpg",
                caption: "BRW",
                addr: "brw.by"
            },
            {
                classItem: "",
                anhor: "autob",
                linkHref: "/projects/autobax",
                linkTitle: "Сайт autobax.by",
                image: "/img/autob/autob.jpg",
                caption: "autobax",
                addr: "autobax.by"
            },
            {
                classItem: "",
                anhor: "mustang",
                linkHref: "/projects/mustang",
                linkTitle: "Сайт mustangtk.com",
                image: "/img/mustang/mustang.jpg",
                caption: "Mustang",
                addr: "mustangtk.com"
            },
            {
                classItem: "",
                anhor: "imns",
                linkHref: "/projects/imns",
                linkTitle: "Сайт info-center.by",
                image: "/img/imns/crop/imns.jpg",
                caption: "ИМНС",
                addr: "info-center.by"
            },
        ]
    },
    {
        portfolioList: [
            {
                classItem: "",
                anhor: "atevi",
                linkHref: "",
                linkTitle: "",
                image: "/img/toresp/brw-shop.jpg",
                caption: "BRW shop",
                addr: "brw-shop.by"
            },
            {
                classItem: "",
                anhor: "uspeh",
                linkHref: "",
                linkTitle: "",
                image: "/img/toresp/n-tyre.jpg",
                caption: "Ntyre",
                addr: "n-tyre.by"
            },
            {
                classItem: "",
                anhor: "imns",
                linkHref: "",
                linkTitle: "",
                image: "/img/toresp/shd.jpg",
                caption: "Шинный двор",
                addr: "shd.by"
            },
            {
                classItem: "",
                anhor: "imns",
                linkHref: "",
                linkTitle: "",
                image: "/img/toresp/upak.jpg",
                caption: "Упак",
                addr: "upak.by"
            },
        ]
    }
]

export default portfolioContent;