const aboutBlockContent = [
    {
        capBlock: {
            titleCaption: "наши проекты"
        },
        textBlock: {
            text: "Мы сверстали, адаптировали для мобильных телефонов и планшетов удобные и продающие <br>web-сайты, которые помогают нашим клиентам добиться успеха"
        }
    },
    {
        capBlock: {
            titleCaption: "Проекты адаптации"
        },
        textBlock: {
            text: "Каким бы не был ваш текущий сайт, в наших силах его оптимизировать <br>и реализовать полную адаптацию для мобильных устройств."
        }
    }
]

export default aboutBlockContent;