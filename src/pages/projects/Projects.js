import React, { Component } from 'react';

import Header from '../../components/siteblocks/Header.js';
import Footer from '../../components/siteblocks/Footer.js';
import AboutBlock from '../../components/siteblocks/AboutBlock.js';
import FootBlock from '../../components/siteblocks/FootBlock.js';
import Portfolio from '../../components/siteblocks/Portfolio.js';
import aboutBlockContent from './aboutBlockContent';
import portfolioContent from './portfolioContent';

class Projects extends Component {
    render() {
        const pageName = "Проекты";
        return (
            <div>
                <Header pageIs={pageName} />
                <AboutBlock content={aboutBlockContent[0]} />
                <Portfolio content={portfolioContent[0]} classToBlock="projectsPage" />
                <section></section>
                <AboutBlock content={aboutBlockContent[1]} />
                <Portfolio content={portfolioContent[1]} classToBlock="projectsPage" />
                <section></section>
                <FootBlock />
                <Footer />
            </div>
        );
    }
}

export default Projects;