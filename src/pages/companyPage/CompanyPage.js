import React, { Component } from 'react';

import Header from '../../components/siteblocks/Header.js';
import Footer from '../../components/siteblocks/Footer.js';
import FootBlock from '../../components/siteblocks/FootBlock.js';
import AboutBlock from '../../components/siteblocks/AboutBlock.js';
import PortfolioProject from '../../components/siteblocks/PortfolioProject.js';
import aboutBlockContent from './aboutBlockContent';
import portfolioProjectContent from './portfolioProjectContent';

class CompanyPage extends Component {
    render() {
        const key = this.props.match.params.company;
        return (
            <div>
                <Header />
                <AboutBlock content={aboutBlockContent[key]} />
                <PortfolioProject content={portfolioProjectContent[key]} />
                <FootBlock />
                <Footer />
            </div>
        );
    }
}

export default CompanyPage;