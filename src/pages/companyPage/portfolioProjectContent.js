const portfolioProjectContent = {
    atevi: {
        desktop: [
            {
                link: "/img/atevi/atevi_00001.jpg",
                linkTitle: "Atevi Главная",
                image: "/img/atevi/crop/atevi_00001.jpg"
            },
            {
                link: "/img/atevi/atevi_00002.jpg",
                linkTitle: "Atevi Внеднение Bitrix",
                image: "/img/atevi/crop/atevi_00002.jpg"
            },
            {
                link: "/img/atevi/atevi_00003.jpg",
                linkTitle: "Atevi Кейс проекта",
                image: "/img/atevi/crop/atevi_00003.jpg"
            },
            {
                link: "/img/atevi/atevi_00004.jpg",
                linkTitle: "Atevi Контакты",
                image: "/img/atevi/crop/atevi_00004.jpg"
            },
            {
                link: "/img/atevi/atevi_00005.jpg",
                linkTitle: "Atevi О компании",
                image: "/img/atevi/crop/atevi_00005.jpg"
            },
            {
                link: "/img/atevi/atevi_00006.jpg",
                linkTitle: "Atevi Поддержка Bitrix",
                image: "/img/atevi/crop/atevi_00006.jpg"
            },
            {
                link: "/img/atevi/atevi_00007.jpg",
                linkTitle: "Atevi Услуга Поддержка",
                image: "/img/atevi/crop/atevi_00007.jpg"
            },
            {
                link: "/img/atevi/atevi_00008.jpg",
                linkTitle: "Atevi Услуга разработки",
                image: "/img/atevi/crop/atevi_00008.jpg"
            },
        ],
        tablet: [
            {
                link: "/img/atevi/atevi_00001_tp.jpg",
                linkTitle: "Atevi Главная планшет",
                image: "/img/atevi/crop/atevi_00001_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00002_tp.jpg",
                linkTitle: "Atevi Внеднение Bitrix планшет",
                image: "/img/atevi/crop/atevi_00002_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00003_tp.jpg",
                linkTitle: "Atevi Кейс проекта планшет",
                image: "/img/atevi/crop/atevi_00003_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00004_tp.jpg",
                linkTitle: "Atevi Контакты планшет",
                image: "/img/atevi/crop/atevi_00004_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00005_tp.jpg",
                linkTitle: "Atevi О компании планшет",
                image: "/img/atevi/crop/atevi_00005_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00006_tp.jpg",
                linkTitle: "Atevi Поддержка Bitrix планшет",
                image: "/img/atevi/crop/atevi_00006_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00007_tp.jpg",
                linkTitle: "Atevi Услуга Поддержка планшет",
                image: "/img/atevi/crop/atevi_00007_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00008_tp.jpg",
                linkTitle: "Atevi Услуга разработки планшет",
                image: "/img/atevi/crop/atevi_00008_tp.jpg"
            },
        ],
        mobile: [
            {
                link: "/img/atevi/atevi_00001_m.jpg",
                linkTitle: "Atevi Главная мобильный телефон",
                image: "/img/atevi/crop/atevi_00001_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00002_m.jpg",
                linkTitle: "Atevi Внеднение Bitrix мобильный телефон",
                image: "/img/atevi/crop/atevi_00002_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00003_m.jpg",
                linkTitle: "Atevi Кейс проекта мобильный телефон",
                image: "/img/atevi/crop/atevi_00003_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00004_m.jpg",
                linkTitle: "Atevi Контакты мобильный телефон",
                image: "/img/atevi/crop/atevi_00004_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00005_m.jpg",
                linkTitle: "Atevi О компании мобильный телефон",
                image: "/img/atevi/crop/atevi_00005_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00006_m.jpg",
                linkTitle: "Atevi Поддержка Bitrix мобильный телефон",
                image: "/img/atevi/crop/atevi_00006_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00007_m.jpg",
                linkTitle: "Atevi Услуга Поддержка мобильный телефон",
                image: "/img/atevi/crop/atevi_00007_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00008_m.jpg",
                linkTitle: "Atevi Услуга разработки мобильный телефон",
                image: "/img/atevi/crop/atevi_00008_m.jpg"
            },
        ],
        textBlock: "Компания <a href='/about'>tinc</a> используя Компонентную структуру элементов полностью Сверстала адаптивный web-сайт &laquo;Atevi Systems&raquo;. <b>Уникальное использование модулей</b> библиотеки jQuety позволяет компактно отображать элементы сайта на <br>мобильном телефоне и планшете."
    },
    uspeh: {
        desktop: [
            {
                link: "/img/atevi/atevi_00001.jpg",
                linkTitle: "Atevi Главная",
                image: "/img/atevi/crop/atevi_00001.jpg"
            },
            {
                link: "/img/atevi/atevi_00002.jpg",
                linkTitle: "Atevi Внеднение Bitrix",
                image: "/img/atevi/crop/atevi_00002.jpg"
            },
            {
                link: "/img/atevi/atevi_00003.jpg",
                linkTitle: "Atevi Кейс проекта",
                image: "/img/atevi/crop/atevi_00003.jpg"
            },
            {
                link: "/img/atevi/atevi_00004.jpg",
                linkTitle: "Atevi Контакты",
                image: "/img/atevi/crop/atevi_00004.jpg"
            },
            {
                link: "/img/atevi/atevi_00005.jpg",
                linkTitle: "Atevi О компании",
                image: "/img/atevi/crop/atevi_00005.jpg"
            },
            {
                link: "/img/atevi/atevi_00006.jpg",
                linkTitle: "Atevi Поддержка Bitrix",
                image: "/img/atevi/crop/atevi_00006.jpg"
            },
            {
                link: "/img/atevi/atevi_00007.jpg",
                linkTitle: "Atevi Услуга Поддержка",
                image: "/img/atevi/crop/atevi_00007.jpg"
            },
            {
                link: "/img/atevi/atevi_00008.jpg",
                linkTitle: "Atevi Услуга разработки",
                image: "/img/atevi/crop/atevi_00008.jpg"
            },
        ],
        tablet: [
            {
                link: "/img/atevi/atevi_00001_tp.jpg",
                linkTitle: "Atevi Главная планшет",
                image: "/img/atevi/crop/atevi_00001_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00002_tp.jpg",
                linkTitle: "Atevi Внеднение Bitrix планшет",
                image: "/img/atevi/crop/atevi_00002_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00003_tp.jpg",
                linkTitle: "Atevi Кейс проекта планшет",
                image: "/img/atevi/crop/atevi_00003_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00004_tp.jpg",
                linkTitle: "Atevi Контакты планшет",
                image: "/img/atevi/crop/atevi_00004_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00005_tp.jpg",
                linkTitle: "Atevi О компании планшет",
                image: "/img/atevi/crop/atevi_00005_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00006_tp.jpg",
                linkTitle: "Atevi Поддержка Bitrix планшет",
                image: "/img/atevi/crop/atevi_00006_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00007_tp.jpg",
                linkTitle: "Atevi Услуга Поддержка планшет",
                image: "/img/atevi/crop/atevi_00007_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00008_tp.jpg",
                linkTitle: "Atevi Услуга разработки планшет",
                image: "/img/atevi/crop/atevi_00008_tp.jpg"
            },
        ],
        mobile: [
            {
                link: "/img/atevi/atevi_00001_m.jpg",
                linkTitle: "Atevi Главная мобильный телефон",
                image: "/img/atevi/crop/atevi_00001_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00002_m.jpg",
                linkTitle: "Atevi Внеднение Bitrix мобильный телефон",
                image: "/img/atevi/crop/atevi_00002_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00003_m.jpg",
                linkTitle: "Atevi Кейс проекта мобильный телефон",
                image: "/img/atevi/crop/atevi_00003_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00004_m.jpg",
                linkTitle: "Atevi Контакты мобильный телефон",
                image: "/img/atevi/crop/atevi_00004_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00005_m.jpg",
                linkTitle: "Atevi О компании мобильный телефон",
                image: "/img/atevi/crop/atevi_00005_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00006_m.jpg",
                linkTitle: "Atevi Поддержка Bitrix мобильный телефон",
                image: "/img/atevi/crop/atevi_00006_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00007_m.jpg",
                linkTitle: "Atevi Услуга Поддержка мобильный телефон",
                image: "/img/atevi/crop/atevi_00007_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00008_m.jpg",
                linkTitle: "Atevi Услуга разработки мобильный телефон",
                image: "/img/atevi/crop/atevi_00008_m.jpg"
            },
        ],
        textBlock: "Адаптивный сайт реализован компанией <a href='/about'>tinc</a> используя технологию <b>Bootstrap 3</b>. Сайт адаптирован для корректного отображения как на мобильном телефоне так и на планшете.<br class='hideMD'>Мы максимально оптимизировали Вёрстку согласно <b>SEO</b> требований к коду."
    },
    hoster: {
        desktop: [
            {
                link: "/img/atevi/atevi_00001.jpg",
                linkTitle: "Atevi Главная",
                image: "/img/atevi/crop/atevi_00001.jpg"
            },
            {
                link: "/img/atevi/atevi_00002.jpg",
                linkTitle: "Atevi Внеднение Bitrix",
                image: "/img/atevi/crop/atevi_00002.jpg"
            },
            {
                link: "/img/atevi/atevi_00003.jpg",
                linkTitle: "Atevi Кейс проекта",
                image: "/img/atevi/crop/atevi_00003.jpg"
            },
            {
                link: "/img/atevi/atevi_00004.jpg",
                linkTitle: "Atevi Контакты",
                image: "/img/atevi/crop/atevi_00004.jpg"
            },
            {
                link: "/img/atevi/atevi_00005.jpg",
                linkTitle: "Atevi О компании",
                image: "/img/atevi/crop/atevi_00005.jpg"
            },
            {
                link: "/img/atevi/atevi_00006.jpg",
                linkTitle: "Atevi Поддержка Bitrix",
                image: "/img/atevi/crop/atevi_00006.jpg"
            },
            {
                link: "/img/atevi/atevi_00007.jpg",
                linkTitle: "Atevi Услуга Поддержка",
                image: "/img/atevi/crop/atevi_00007.jpg"
            },
            {
                link: "/img/atevi/atevi_00008.jpg",
                linkTitle: "Atevi Услуга разработки",
                image: "/img/atevi/crop/atevi_00008.jpg"
            },
        ],
        tablet: [
            {
                link: "/img/atevi/atevi_00001_tp.jpg",
                linkTitle: "Atevi Главная планшет",
                image: "/img/atevi/crop/atevi_00001_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00002_tp.jpg",
                linkTitle: "Atevi Внеднение Bitrix планшет",
                image: "/img/atevi/crop/atevi_00002_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00003_tp.jpg",
                linkTitle: "Atevi Кейс проекта планшет",
                image: "/img/atevi/crop/atevi_00003_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00004_tp.jpg",
                linkTitle: "Atevi Контакты планшет",
                image: "/img/atevi/crop/atevi_00004_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00005_tp.jpg",
                linkTitle: "Atevi О компании планшет",
                image: "/img/atevi/crop/atevi_00005_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00006_tp.jpg",
                linkTitle: "Atevi Поддержка Bitrix планшет",
                image: "/img/atevi/crop/atevi_00006_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00007_tp.jpg",
                linkTitle: "Atevi Услуга Поддержка планшет",
                image: "/img/atevi/crop/atevi_00007_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00008_tp.jpg",
                linkTitle: "Atevi Услуга разработки планшет",
                image: "/img/atevi/crop/atevi_00008_tp.jpg"
            },
        ],
        mobile: [
            {
                link: "/img/atevi/atevi_00001_m.jpg",
                linkTitle: "Atevi Главная мобильный телефон",
                image: "/img/atevi/crop/atevi_00001_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00002_m.jpg",
                linkTitle: "Atevi Внеднение Bitrix мобильный телефон",
                image: "/img/atevi/crop/atevi_00002_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00003_m.jpg",
                linkTitle: "Atevi Кейс проекта мобильный телефон",
                image: "/img/atevi/crop/atevi_00003_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00004_m.jpg",
                linkTitle: "Atevi Контакты мобильный телефон",
                image: "/img/atevi/crop/atevi_00004_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00005_m.jpg",
                linkTitle: "Atevi О компании мобильный телефон",
                image: "/img/atevi/crop/atevi_00005_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00006_m.jpg",
                linkTitle: "Atevi Поддержка Bitrix мобильный телефон",
                image: "/img/atevi/crop/atevi_00006_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00007_m.jpg",
                linkTitle: "Atevi Услуга Поддержка мобильный телефон",
                image: "/img/atevi/crop/atevi_00007_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00008_m.jpg",
                linkTitle: "Atevi Услуга разработки мобильный телефон",
                image: "/img/atevi/crop/atevi_00008_m.jpg"
            },
        ],
        textBlock: "Компания <a href='/about'>tinc</a> используя Компонентную структуру элементов полностью Сверстала адаптивный web-сайт &laquo;hoster Systems&raquo;. <b>Уникальное использование модулей</b> библиотеки jQuety позволяет компактно отображать элементы сайта на <br>мобильном телефоне и планшете."
    },
    teamwork: {
        desktop: [
            {
                link: "/img/atevi/atevi_00001.jpg",
                linkTitle: "Atevi Главная",
                image: "/img/atevi/crop/atevi_00001.jpg"
            },
            {
                link: "/img/atevi/atevi_00002.jpg",
                linkTitle: "Atevi Внеднение Bitrix",
                image: "/img/atevi/crop/atevi_00002.jpg"
            },
            {
                link: "/img/atevi/atevi_00003.jpg",
                linkTitle: "Atevi Кейс проекта",
                image: "/img/atevi/crop/atevi_00003.jpg"
            },
            {
                link: "/img/atevi/atevi_00004.jpg",
                linkTitle: "Atevi Контакты",
                image: "/img/atevi/crop/atevi_00004.jpg"
            },
            {
                link: "/img/atevi/atevi_00005.jpg",
                linkTitle: "Atevi О компании",
                image: "/img/atevi/crop/atevi_00005.jpg"
            },
            {
                link: "/img/atevi/atevi_00006.jpg",
                linkTitle: "Atevi Поддержка Bitrix",
                image: "/img/atevi/crop/atevi_00006.jpg"
            },
            {
                link: "/img/atevi/atevi_00007.jpg",
                linkTitle: "Atevi Услуга Поддержка",
                image: "/img/atevi/crop/atevi_00007.jpg"
            },
            {
                link: "/img/atevi/atevi_00008.jpg",
                linkTitle: "Atevi Услуга разработки",
                image: "/img/atevi/crop/atevi_00008.jpg"
            },
        ],
        tablet: [
            {
                link: "/img/atevi/atevi_00001_tp.jpg",
                linkTitle: "Atevi Главная планшет",
                image: "/img/atevi/crop/atevi_00001_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00002_tp.jpg",
                linkTitle: "Atevi Внеднение Bitrix планшет",
                image: "/img/atevi/crop/atevi_00002_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00003_tp.jpg",
                linkTitle: "Atevi Кейс проекта планшет",
                image: "/img/atevi/crop/atevi_00003_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00004_tp.jpg",
                linkTitle: "Atevi Контакты планшет",
                image: "/img/atevi/crop/atevi_00004_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00005_tp.jpg",
                linkTitle: "Atevi О компании планшет",
                image: "/img/atevi/crop/atevi_00005_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00006_tp.jpg",
                linkTitle: "Atevi Поддержка Bitrix планшет",
                image: "/img/atevi/crop/atevi_00006_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00007_tp.jpg",
                linkTitle: "Atevi Услуга Поддержка планшет",
                image: "/img/atevi/crop/atevi_00007_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00008_tp.jpg",
                linkTitle: "Atevi Услуга разработки планшет",
                image: "/img/atevi/crop/atevi_00008_tp.jpg"
            },
        ],
        mobile: [
            {
                link: "/img/atevi/atevi_00001_m.jpg",
                linkTitle: "Atevi Главная мобильный телефон",
                image: "/img/atevi/crop/atevi_00001_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00002_m.jpg",
                linkTitle: "Atevi Внеднение Bitrix мобильный телефон",
                image: "/img/atevi/crop/atevi_00002_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00003_m.jpg",
                linkTitle: "Atevi Кейс проекта мобильный телефон",
                image: "/img/atevi/crop/atevi_00003_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00004_m.jpg",
                linkTitle: "Atevi Контакты мобильный телефон",
                image: "/img/atevi/crop/atevi_00004_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00005_m.jpg",
                linkTitle: "Atevi О компании мобильный телефон",
                image: "/img/atevi/crop/atevi_00005_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00006_m.jpg",
                linkTitle: "Atevi Поддержка Bitrix мобильный телефон",
                image: "/img/atevi/crop/atevi_00006_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00007_m.jpg",
                linkTitle: "Atevi Услуга Поддержка мобильный телефон",
                image: "/img/atevi/crop/atevi_00007_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00008_m.jpg",
                linkTitle: "Atevi Услуга разработки мобильный телефон",
                image: "/img/atevi/crop/atevi_00008_m.jpg"
            },
        ],
        textBlock: "Для web-сайт компании &laquo;TeamWork&raquo; мы реализовали <br class='hideMD'>уникальный Адаптивный Дизайн и Компонентную Вёрстку, а также <br class='hideMD'>Компания <a href='/'>tinc</a> запрограммировала web-сайт на <b>framework</b> <br> <img src='/img/react.png' alt='reactJS'>"
    },
    brw: {
        desktop: [
            {
                link: "/img/atevi/atevi_00001.jpg",
                linkTitle: "Atevi Главная",
                image: "/img/atevi/crop/atevi_00001.jpg"
            },
            {
                link: "/img/atevi/atevi_00002.jpg",
                linkTitle: "Atevi Внеднение Bitrix",
                image: "/img/atevi/crop/atevi_00002.jpg"
            },
            {
                link: "/img/atevi/atevi_00003.jpg",
                linkTitle: "Atevi Кейс проекта",
                image: "/img/atevi/crop/atevi_00003.jpg"
            },
            {
                link: "/img/atevi/atevi_00004.jpg",
                linkTitle: "Atevi Контакты",
                image: "/img/atevi/crop/atevi_00004.jpg"
            },
            {
                link: "/img/atevi/atevi_00005.jpg",
                linkTitle: "Atevi О компании",
                image: "/img/atevi/crop/atevi_00005.jpg"
            },
            {
                link: "/img/atevi/atevi_00006.jpg",
                linkTitle: "Atevi Поддержка Bitrix",
                image: "/img/atevi/crop/atevi_00006.jpg"
            },
            {
                link: "/img/atevi/atevi_00007.jpg",
                linkTitle: "Atevi Услуга Поддержка",
                image: "/img/atevi/crop/atevi_00007.jpg"
            },
            {
                link: "/img/atevi/atevi_00008.jpg",
                linkTitle: "Atevi Услуга разработки",
                image: "/img/atevi/crop/atevi_00008.jpg"
            },
        ],
        tablet: [
            {
                link: "/img/atevi/atevi_00001_tp.jpg",
                linkTitle: "Atevi Главная планшет",
                image: "/img/atevi/crop/atevi_00001_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00002_tp.jpg",
                linkTitle: "Atevi Внеднение Bitrix планшет",
                image: "/img/atevi/crop/atevi_00002_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00003_tp.jpg",
                linkTitle: "Atevi Кейс проекта планшет",
                image: "/img/atevi/crop/atevi_00003_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00004_tp.jpg",
                linkTitle: "Atevi Контакты планшет",
                image: "/img/atevi/crop/atevi_00004_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00005_tp.jpg",
                linkTitle: "Atevi О компании планшет",
                image: "/img/atevi/crop/atevi_00005_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00006_tp.jpg",
                linkTitle: "Atevi Поддержка Bitrix планшет",
                image: "/img/atevi/crop/atevi_00006_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00007_tp.jpg",
                linkTitle: "Atevi Услуга Поддержка планшет",
                image: "/img/atevi/crop/atevi_00007_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00008_tp.jpg",
                linkTitle: "Atevi Услуга разработки планшет",
                image: "/img/atevi/crop/atevi_00008_tp.jpg"
            },
        ],
        mobile: [
            {
                link: "/img/atevi/atevi_00001_m.jpg",
                linkTitle: "Atevi Главная мобильный телефон",
                image: "/img/atevi/crop/atevi_00001_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00002_m.jpg",
                linkTitle: "Atevi Внеднение Bitrix мобильный телефон",
                image: "/img/atevi/crop/atevi_00002_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00003_m.jpg",
                linkTitle: "Atevi Кейс проекта мобильный телефон",
                image: "/img/atevi/crop/atevi_00003_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00004_m.jpg",
                linkTitle: "Atevi Контакты мобильный телефон",
                image: "/img/atevi/crop/atevi_00004_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00005_m.jpg",
                linkTitle: "Atevi О компании мобильный телефон",
                image: "/img/atevi/crop/atevi_00005_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00006_m.jpg",
                linkTitle: "Atevi Поддержка Bitrix мобильный телефон",
                image: "/img/atevi/crop/atevi_00006_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00007_m.jpg",
                linkTitle: "Atevi Услуга Поддержка мобильный телефон",
                image: "/img/atevi/crop/atevi_00007_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00008_m.jpg",
                linkTitle: "Atevi Услуга разработки мобильный телефон",
                image: "/img/atevi/crop/atevi_00008_m.jpg"
            },
        ],
        textBlock: "Для &laquo;Black Red White&raquo; компания <a href='/about'>tinc</a> реализовала адаптивную и кроссбраузернуй Вёрстку. <br class='hideMD'>Для достижения максимально удобного интерфейса на мобильном телефоне <br class='hideMD'>мы разработали <b>компонент плавающих элементов</b>, <br class='hideMD'>что позволило избежать дублирование блоков на web-сайте."
    },
    autobax: {
        desktop: [
            {
                link: "/img/atevi/atevi_00001.jpg",
                linkTitle: "Atevi Главная",
                image: "/img/atevi/crop/atevi_00001.jpg"
            },
            {
                link: "/img/atevi/atevi_00002.jpg",
                linkTitle: "Atevi Внеднение Bitrix",
                image: "/img/atevi/crop/atevi_00002.jpg"
            },
            {
                link: "/img/atevi/atevi_00003.jpg",
                linkTitle: "Atevi Кейс проекта",
                image: "/img/atevi/crop/atevi_00003.jpg"
            },
            {
                link: "/img/atevi/atevi_00004.jpg",
                linkTitle: "Atevi Контакты",
                image: "/img/atevi/crop/atevi_00004.jpg"
            },
            {
                link: "/img/atevi/atevi_00005.jpg",
                linkTitle: "Atevi О компании",
                image: "/img/atevi/crop/atevi_00005.jpg"
            },
            {
                link: "/img/atevi/atevi_00006.jpg",
                linkTitle: "Atevi Поддержка Bitrix",
                image: "/img/atevi/crop/atevi_00006.jpg"
            },
            {
                link: "/img/atevi/atevi_00007.jpg",
                linkTitle: "Atevi Услуга Поддержка",
                image: "/img/atevi/crop/atevi_00007.jpg"
            },
            {
                link: "/img/atevi/atevi_00008.jpg",
                linkTitle: "Atevi Услуга разработки",
                image: "/img/atevi/crop/atevi_00008.jpg"
            },
        ],
        tablet: [
            {
                link: "/img/atevi/atevi_00001_tp.jpg",
                linkTitle: "Atevi Главная планшет",
                image: "/img/atevi/crop/atevi_00001_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00002_tp.jpg",
                linkTitle: "Atevi Внеднение Bitrix планшет",
                image: "/img/atevi/crop/atevi_00002_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00003_tp.jpg",
                linkTitle: "Atevi Кейс проекта планшет",
                image: "/img/atevi/crop/atevi_00003_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00004_tp.jpg",
                linkTitle: "Atevi Контакты планшет",
                image: "/img/atevi/crop/atevi_00004_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00005_tp.jpg",
                linkTitle: "Atevi О компании планшет",
                image: "/img/atevi/crop/atevi_00005_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00006_tp.jpg",
                linkTitle: "Atevi Поддержка Bitrix планшет",
                image: "/img/atevi/crop/atevi_00006_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00007_tp.jpg",
                linkTitle: "Atevi Услуга Поддержка планшет",
                image: "/img/atevi/crop/atevi_00007_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00008_tp.jpg",
                linkTitle: "Atevi Услуга разработки планшет",
                image: "/img/atevi/crop/atevi_00008_tp.jpg"
            },
        ],
        mobile: [
            {
                link: "/img/atevi/atevi_00001_m.jpg",
                linkTitle: "Atevi Главная мобильный телефон",
                image: "/img/atevi/crop/atevi_00001_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00002_m.jpg",
                linkTitle: "Atevi Внеднение Bitrix мобильный телефон",
                image: "/img/atevi/crop/atevi_00002_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00003_m.jpg",
                linkTitle: "Atevi Кейс проекта мобильный телефон",
                image: "/img/atevi/crop/atevi_00003_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00004_m.jpg",
                linkTitle: "Atevi Контакты мобильный телефон",
                image: "/img/atevi/crop/atevi_00004_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00005_m.jpg",
                linkTitle: "Atevi О компании мобильный телефон",
                image: "/img/atevi/crop/atevi_00005_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00006_m.jpg",
                linkTitle: "Atevi Поддержка Bitrix мобильный телефон",
                image: "/img/atevi/crop/atevi_00006_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00007_m.jpg",
                linkTitle: "Atevi Услуга Поддержка мобильный телефон",
                image: "/img/atevi/crop/atevi_00007_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00008_m.jpg",
                linkTitle: "Atevi Услуга разработки мобильный телефон",
                image: "/img/atevi/crop/atevi_00008_m.jpg"
            },
        ],
        textBlock: "Используя библиотеку <b>Bootstrap 3</b> компания <a href='/about'>tinc</a> реализовала полностью адаптивную и кроссбраузерную Вёрстку для интернет-магазина &laquo;Автобакс&raquo;. На сайте реализован полноценный framework анимации используя технологии <b>CSS 3</b>."
    },
    mustang: {
        desktop: [
            {
                link: "/img/atevi/atevi_00001.jpg",
                linkTitle: "Atevi Главная",
                image: "/img/atevi/crop/atevi_00001.jpg"
            },
            {
                link: "/img/atevi/atevi_00002.jpg",
                linkTitle: "Atevi Внеднение Bitrix",
                image: "/img/atevi/crop/atevi_00002.jpg"
            },
            {
                link: "/img/atevi/atevi_00003.jpg",
                linkTitle: "Atevi Кейс проекта",
                image: "/img/atevi/crop/atevi_00003.jpg"
            },
            {
                link: "/img/atevi/atevi_00004.jpg",
                linkTitle: "Atevi Контакты",
                image: "/img/atevi/crop/atevi_00004.jpg"
            },
            {
                link: "/img/atevi/atevi_00005.jpg",
                linkTitle: "Atevi О компании",
                image: "/img/atevi/crop/atevi_00005.jpg"
            },
            {
                link: "/img/atevi/atevi_00006.jpg",
                linkTitle: "Atevi Поддержка Bitrix",
                image: "/img/atevi/crop/atevi_00006.jpg"
            },
            {
                link: "/img/atevi/atevi_00007.jpg",
                linkTitle: "Atevi Услуга Поддержка",
                image: "/img/atevi/crop/atevi_00007.jpg"
            },
            {
                link: "/img/atevi/atevi_00008.jpg",
                linkTitle: "Atevi Услуга разработки",
                image: "/img/atevi/crop/atevi_00008.jpg"
            },
        ],
        tablet: [
            {
                link: "/img/atevi/atevi_00001_tp.jpg",
                linkTitle: "Atevi Главная планшет",
                image: "/img/atevi/crop/atevi_00001_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00002_tp.jpg",
                linkTitle: "Atevi Внеднение Bitrix планшет",
                image: "/img/atevi/crop/atevi_00002_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00003_tp.jpg",
                linkTitle: "Atevi Кейс проекта планшет",
                image: "/img/atevi/crop/atevi_00003_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00004_tp.jpg",
                linkTitle: "Atevi Контакты планшет",
                image: "/img/atevi/crop/atevi_00004_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00005_tp.jpg",
                linkTitle: "Atevi О компании планшет",
                image: "/img/atevi/crop/atevi_00005_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00006_tp.jpg",
                linkTitle: "Atevi Поддержка Bitrix планшет",
                image: "/img/atevi/crop/atevi_00006_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00007_tp.jpg",
                linkTitle: "Atevi Услуга Поддержка планшет",
                image: "/img/atevi/crop/atevi_00007_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00008_tp.jpg",
                linkTitle: "Atevi Услуга разработки планшет",
                image: "/img/atevi/crop/atevi_00008_tp.jpg"
            },
        ],
        mobile: [
            {
                link: "/img/atevi/atevi_00001_m.jpg",
                linkTitle: "Atevi Главная мобильный телефон",
                image: "/img/atevi/crop/atevi_00001_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00002_m.jpg",
                linkTitle: "Atevi Внеднение Bitrix мобильный телефон",
                image: "/img/atevi/crop/atevi_00002_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00003_m.jpg",
                linkTitle: "Atevi Кейс проекта мобильный телефон",
                image: "/img/atevi/crop/atevi_00003_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00004_m.jpg",
                linkTitle: "Atevi Контакты мобильный телефон",
                image: "/img/atevi/crop/atevi_00004_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00005_m.jpg",
                linkTitle: "Atevi О компании мобильный телефон",
                image: "/img/atevi/crop/atevi_00005_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00006_m.jpg",
                linkTitle: "Atevi Поддержка Bitrix мобильный телефон",
                image: "/img/atevi/crop/atevi_00006_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00007_m.jpg",
                linkTitle: "Atevi Услуга Поддержка мобильный телефон",
                image: "/img/atevi/crop/atevi_00007_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00008_m.jpg",
                linkTitle: "Atevi Услуга разработки мобильный телефон",
                image: "/img/atevi/crop/atevi_00008_m.jpg"
            },
        ],
        textBlock: "Сайт реализован как полноценное компонентное решение используя следующие <b>технологии</b>: Библиотека jQuery; Модуль карты Google Maps Api. Для исключения ошибок мы адаптировали Вёрстку каждого компонента для мобильного телефона и планшета в отдельности."
    },
    imns: {
        desktop: [
            {
                link: "/img/atevi/atevi_00001.jpg",
                linkTitle: "Atevi Главная",
                image: "/img/atevi/crop/atevi_00001.jpg"
            },
            {
                link: "/img/atevi/atevi_00002.jpg",
                linkTitle: "Atevi Внеднение Bitrix",
                image: "/img/atevi/crop/atevi_00002.jpg"
            },
            {
                link: "/img/atevi/atevi_00003.jpg",
                linkTitle: "Atevi Кейс проекта",
                image: "/img/atevi/crop/atevi_00003.jpg"
            },
            {
                link: "/img/atevi/atevi_00004.jpg",
                linkTitle: "Atevi Контакты",
                image: "/img/atevi/crop/atevi_00004.jpg"
            },
            {
                link: "/img/atevi/atevi_00005.jpg",
                linkTitle: "Atevi О компании",
                image: "/img/atevi/crop/atevi_00005.jpg"
            },
            {
                link: "/img/atevi/atevi_00006.jpg",
                linkTitle: "Atevi Поддержка Bitrix",
                image: "/img/atevi/crop/atevi_00006.jpg"
            },
            {
                link: "/img/atevi/atevi_00007.jpg",
                linkTitle: "Atevi Услуга Поддержка",
                image: "/img/atevi/crop/atevi_00007.jpg"
            },
            {
                link: "/img/atevi/atevi_00008.jpg",
                linkTitle: "Atevi Услуга разработки",
                image: "/img/atevi/crop/atevi_00008.jpg"
            },
        ],
        tablet: [
            {
                link: "/img/atevi/atevi_00001_tp.jpg",
                linkTitle: "Atevi Главная планшет",
                image: "/img/atevi/crop/atevi_00001_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00002_tp.jpg",
                linkTitle: "Atevi Внеднение Bitrix планшет",
                image: "/img/atevi/crop/atevi_00002_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00003_tp.jpg",
                linkTitle: "Atevi Кейс проекта планшет",
                image: "/img/atevi/crop/atevi_00003_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00004_tp.jpg",
                linkTitle: "Atevi Контакты планшет",
                image: "/img/atevi/crop/atevi_00004_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00005_tp.jpg",
                linkTitle: "Atevi О компании планшет",
                image: "/img/atevi/crop/atevi_00005_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00006_tp.jpg",
                linkTitle: "Atevi Поддержка Bitrix планшет",
                image: "/img/atevi/crop/atevi_00006_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00007_tp.jpg",
                linkTitle: "Atevi Услуга Поддержка планшет",
                image: "/img/atevi/crop/atevi_00007_tp.jpg"
            },
            {
                link: "/img/atevi/atevi_00008_tp.jpg",
                linkTitle: "Atevi Услуга разработки планшет",
                image: "/img/atevi/crop/atevi_00008_tp.jpg"
            },
        ],
        mobile: [
            {
                link: "/img/atevi/atevi_00001_m.jpg",
                linkTitle: "Atevi Главная мобильный телефон",
                image: "/img/atevi/crop/atevi_00001_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00002_m.jpg",
                linkTitle: "Atevi Внеднение Bitrix мобильный телефон",
                image: "/img/atevi/crop/atevi_00002_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00003_m.jpg",
                linkTitle: "Atevi Кейс проекта мобильный телефон",
                image: "/img/atevi/crop/atevi_00003_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00004_m.jpg",
                linkTitle: "Atevi Контакты мобильный телефон",
                image: "/img/atevi/crop/atevi_00004_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00005_m.jpg",
                linkTitle: "Atevi О компании мобильный телефон",
                image: "/img/atevi/crop/atevi_00005_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00006_m.jpg",
                linkTitle: "Atevi Поддержка Bitrix мобильный телефон",
                image: "/img/atevi/crop/atevi_00006_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00007_m.jpg",
                linkTitle: "Atevi Услуга Поддержка мобильный телефон",
                image: "/img/atevi/crop/atevi_00007_m.jpg"
            },
            {
                link: "/img/atevi/atevi_00008_m.jpg",
                linkTitle: "Atevi Услуга разработки мобильный телефон",
                image: "/img/atevi/crop/atevi_00008_m.jpg"
            },
        ],
        textBlock: "Компания <a href='/about'>tinc</a> реализовала Вёрстку эксклюзивного Дизайна web-сайта <br class='hideMD'>&laquo;Информационно—издательский центр по налогам и сборам&raquo;. Для настройки всего функционала web-сайта мы использовали такие библиотека как: <b>Bootstrap</b>, <b>Owl Carousel</b>, <b>FancyBox</b>."
    },
}

export default portfolioProjectContent;