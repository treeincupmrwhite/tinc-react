const aboutBlockContent = {
    atevi: {
        capBlock: {
            beforeCaption: "компания",
            titleCaption: "Atevi Systems"
        },
        textBlock: {
            text: "Сайт компании «Atevi Systems» <strong>по разработке, поддержке и проектному внедрению</strong> на платформе 1С-Битрикс и Битрикс24"
        },
        companyLogo: {
            image: "/img/atevi_logo.png"
        }
    },
    uspeh: {
        capBlock: {
            beforeCaption: "ЦКО",
            titleCaption: "Успех"
        },
        textBlock: {
            text: "Эффективные образовательные курсы для взрослых от <br class='hideMD'>Центра Компьютерного Образования «<a href='https://uspehcours.by' target='_blank'>Успех</a>»"
        },
        companyLogo: {
            image: "/img/uspeh_logo.png"
        }
    },
    hoster: {
        capBlock: {
            beforeCaption: "компания",
            titleCaption: "Atevi Systems"
        },
        textBlock: {
            text: "Сайт компании «Atevi Systems» <strong>по разработке, поддержке и проектному внедрению</strong> на платформе 1С-Битрикс и Битрикс24"
        },
        companyLogo: {
            image: "/img/atevi_logo.png"
        }
    },
    teamwork: {
        capBlock: {
            beforeCaption: "студия",
            titleCaption: "Teamwork"
        },
        textBlock: {
            text: "Корпоративный сайт Дизайн-студии по созданию и продвижению web-сайтов, <br class='hideMD'>разработки фирменного стиля и дизайна полиграфических материалов"
        },
        companyLogo: {
            image: "/img/tw_logo.png"
        }
    },
    brw: {
        capBlock: {
            beforeCaption: "корпоративный сайт",
            titleCaption: "Black Red White"
        },
        textBlock: {
            text: "Адаптивный и Кроссбраузерный корпоративный сайт мебели, <br class='hideMD'>предметов интерьера и аксессуаров от Black Red White."
        },
        companyLogo: {
            image: "/img/atevi_logo.png"
        }
    },
    autobax: {
        capBlock: {
            beforeCaption: "Интернет-магазин",
            titleCaption: "Autobax"
        },
        textBlock: {
            text: "Адаптивный интернет-магазин автозапчастей"
        },
        companyLogo: {
            image: "/img/ww_logo.png"
        }
    },
    mustang: {
        capBlock: {
            beforeCaption: "питание",
            titleCaption: "Mustang"
        },
        textBlock: {
            text: "Корпоративный сайт компании комплексных решений <br class='hideMD'>в области питания животных"
        },
        companyLogo: {
            image: "/img/nw_logo.png"
        }
    },
    imns: {
        capBlock: {
            beforeCaption: "инфо-центр",
            titleCaption: "ИМНС"
        },
        textBlock: {
            text: "Корпоративный сайт Республиканского унитарного предприятия налогов и сборов Информационно—издательского центра"
        },
        companyLogo: {
            image: "/img/atevi_logo.png"
        }
    }
}

export default aboutBlockContent;