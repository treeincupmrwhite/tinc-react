const servicesContent = [
    {
        openDataBlock: {
            icon: "icon-responsive",
            beforeCaption: "все устройства",
            caption: "Адаптивная <br class='hideMD'>вёрстка"
        },
        moreData: {
            type: "parametres",
            devices: [
                {icon: "icon-desktop"},
                {icon: "icon-laptop"},
                {icon: "icon-tablet2"},
                {icon: "icon-phone2"},
            ],
            params: [
                {
                    caption: "Время разработки:",
                    numbers: "<b>12</b> - <b>160</b> часов"
                },
                {
                    caption: "Сроки разработки:",
                    numbers: "<b>2</b> -<b>25</b> дней"
                },
                {
                    costClass: "costParam",
                    caption: "Цена часа:",
                    numbers: "28 <span>б.р.</span>"
                }
            ]
        }
    },
    {
        openDataBlock: {
            icon: "icon-to-responsive",
            beforeCaption: "2-я жизнь сайту",
            caption: "Адаптация <br class='hideMD'>сайта"
        },
        moreData: {
            type: "parametres",
            devices: [
                {icon: "icon-desktop"},
                {icon: "icon-laptop"},
                {icon: "icon-tablet2"},
                {icon: "icon-phone2"},
            ],
            params: [
                {
                    caption: "Время разработки:",
                    numbers: "<b>24</b> - <b>80</b> часов"
                },
                {
                    caption: "Сроки разработки:",
                    numbers: "<b>5</b> -<b>16</b> дней"
                },
                {
                    costClass: "costParam",
                    caption: "Цена часа:",
                    numbers: "22 <span>б.р.</span>"
                }
            ]
        }
    },
    {
        openDataBlock: {
            icon: "icon-phone-portrait",
            beforeCaption: "конкретное ус-во",
            caption: "Вёрстка для <br class='hideMD'>устройств"
        },
        moreData: {
            type: "parametres",
            devices: [
                {icon: "icon-tablet2"},
                {icon: "icon-phone2"},
            ],
            params: [
                {
                    caption: "Время разработки:",
                    numbers: "<b>12</b> - <b>160</b> часов"
                },
                {
                    caption: "Сроки разработки:",
                    numbers: "<b>2</b> -<b>25</b> дней"
                },
                {
                    costClass: "costParam",
                    caption: "Цена часа:",
                    numbers: "24 <span>б.р.</span>"
                }
            ]
        }
    },
    {
        openDataBlock: {
            icon: "icon-resize",
            beforeCaption: "для компьютера",
            caption: "Статичная <br class='hideMD'>вёрстка"
        },
        moreData: {
            type: "parametres",
            devices: [
                {icon: "icon-desktop"},
                {icon: "icon-laptop"},
            ],
            params: [
                {
                    caption: "Время разработки:",
                    numbers: "<b>12</b> - <b>56</b> часов"
                },
                {
                    caption: "Сроки разработки:",
                    numbers: "<b>2</b> -<b>8</b> дней"
                },
                {
                    costClass: "costParam",
                    caption: "Цена часа:",
                    numbers: "20 <span>б.р.</span>"
                }
            ]
        }
    },
    {
        openDataBlock: {
            icon: "icon-clipboard",
            beforeCaption: "всё не так плохо",
            caption: "Оптимизация <br class='hideMD'>сайта"
        },
        moreData: {
            type: "parametres",
            devices: [
                {icon: "icon-html"},
                {icon: "icon-css"},
                {icon: "icon-firefox"},
                {icon: "icon-IE"},
                {icon: "icon-tablet2"},
                {icon: "icon-phone2"},
            ],
            params: [
                {
                    caption: "Время разработки:",
                    numbers: "<b>12</b> - <b>58</b> часов"
                },
                {
                    caption: "Сроки разработки:",
                    numbers: "<b>2</b> -<b>5</b> дней"
                },
                {
                    costClass: "costParam",
                    caption: "Цена часа:",
                    numbers: "20 <span>б.р.</span>"
                }
            ]
        }
    },
    {
        openDataBlock: {
            icon: "icon-quest",
            beforeCaption: "что-то новое",
            caption: "Иной род <br class='hideMD'>вёрстки"
        },
        moreData: {
            type: "text",
            text: "Все сайты состоят из тэгов и стилей и возможно именно вам нужно <br class='hideMD'>что-то иное, новое и в силах <br class='hideMD'>Верстальщика.",
        },
    }
]

export default servicesContent