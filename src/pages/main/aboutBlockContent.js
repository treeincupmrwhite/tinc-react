const aboutBlockContent = {
    capBlock: {
        beforeCaption: "Что это такое",
        titleCaption: "Важный этап разработки"
    },
    textBlock: {
        text: "Во время Вёрстки формируется весь внешний вид вашего будущего сайта. <br class='hideMD'>Именно Верстальщик превращает дизайн в реальный сайт.",
        link: "Узнать больше",
        hrefLink: "/about"
    }
}

export default aboutBlockContent;