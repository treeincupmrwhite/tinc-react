import React, { Component } from 'react';

import Header from '../../components/siteblocks/Header.js';
import Footer from '../../components/siteblocks/Footer.js';
import AboutBlock from '../../components/siteblocks/AboutBlock.js';
import Partners from '../../components/siteblocks/Partners.js';
import FootBlock from '../../components/siteblocks/FootBlock.js';
import Portfolio from '../../components/siteblocks/Portfolio.js';
import Pluses from '../../components/siteblocks/Pluses';
import aboutBlockContent from './aboutBlockContent';
import portfolioContent from './portfolioContent';
import servicesContent from './servicesContent';
import textBlock from './textBlock';

class Main extends Component {
    render() {
        const pageName = "Главная";
        return (
            <div>
                <Header pageIs={pageName} />
                <AboutBlock content={aboutBlockContent} />
                <Pluses classToBlock="services" textBlock={textBlock} content={servicesContent} />
                <Portfolio content={portfolioContent} classToBlock="grayBack" />
                <Partners />
                <FootBlock />
                <Footer />
            </div>
        );
    }
}

export default Main;